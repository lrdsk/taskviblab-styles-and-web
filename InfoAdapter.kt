package com.example.fragment_vibelabprj

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.fragment_vibelabprj.databinding.ElemItemBinding

class InfoAdapter: RecyclerView.Adapter<InfoAdapter.InfoHolder>() {
    val elemList = ArrayList<ElementOfRecycler>()
    class InfoHolder(item: View): RecyclerView.ViewHolder(item) {
        val binding = ElemItemBinding.bind(item)
        fun bind(elem: ElementOfRecycler) = with(binding){

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InfoHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.elem_item, parent, false)
        return InfoHolder(view)
    }

    override fun onBindViewHolder(holder: InfoHolder, position: Int) {
        holder.bind(elemList[position])
    }

    override fun getItemCount(): Int {
        return elemList.size
    }
    fun addElem(elem: ElementOfRecycler){
        elemList.add(elem)
        notifyDataSetChanged()
    }
}
