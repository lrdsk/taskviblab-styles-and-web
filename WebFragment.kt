package com.example.fragment_vibelabprj.fragments

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi
import com.example.fragment_vibelabprj.databinding.FragmentWebBinding


class WebFragment : Fragment() {
    lateinit var bindingClass: FragmentWebBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        bindingClass = FragmentWebBinding.inflate(inflater)
        webViewSetup()
        return bindingClass.root
    }
    private fun webViewSetup(){
        bindingClass.wbWebView.webViewClient = WebViewClient()

        bindingClass.wbWebView.apply {
            loadUrl("https://www.google.com/")
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = WebFragment()
    }
}
