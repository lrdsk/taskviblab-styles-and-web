package com.example.fragment_vibelabprj.fragments

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.fragment_vibelabprj.ElementOfRecycler
import com.example.fragment_vibelabprj.InfoAdapter
import com.example.fragment_vibelabprj.MainActivity
import com.example.fragment_vibelabprj.R
import com.example.fragment_vibelabprj.databinding.FragmentHomeBinding


class HomeFragment : Fragment() {
    lateinit var bindingClass: FragmentHomeBinding
    private val mainActivity = MainActivity()
    private val adapter = InfoAdapter()
    val listTitle = listOf("Title1", "Title2", "Title3", "Title4", "Title5")
    private var index = 0
    private var isClick = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bindingClass = FragmentHomeBinding.inflate(inflater)

        bindingClass.bNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.goToLogin -> {
                    Navigation.findNavController(bindingClass.root)
                        .navigate(R.id.navigateToLoginFragment)
                }
                R.id.goToWebFragment -> {
                    Navigation.findNavController(bindingClass.root)
                        .navigate(R.id.navigateToWebFragment)
                }
            }
            true
        }
        bindingClass.bChangeStyle.setOnClickListener {
            if (isClick == 0) {
                bindingClass.tvWelcome.textSize = 30f
                bindingClass.tvWelcome.setTextColor(Color.GRAY)
                isClick = 1
            } else {

                bindingClass.tvWelcome.textSize = 24f
                bindingClass.tvWelcome.setTextColor(Color.WHITE)
                isClick = 0
            }
        }
        init()

        return bindingClass.root
    }

    private fun init() {
        bindingClass.rcView.layoutManager = GridLayoutManager(mainActivity, 3)
        bindingClass.rcView.adapter = adapter
        for (index in 0..4) {
            val elem = ElementOfRecycler(listTitle[index])
            adapter.addElem(elem)
        }

    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) = HomeFragment()
    }
}
